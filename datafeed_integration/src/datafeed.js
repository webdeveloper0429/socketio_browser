import express from 'express';
import http from 'http';
import sio from 'socket.io';

import schedule from 'node-schedule';
import autobahn from 'autobahn';

import routes from './routes';

const app = express();
const server = http.createServer(app);
// const io = sio.listen(server);
const io = sio.listen('1235');

app.use(express.static(__dirname + '/public'));

const port = process.env.PORT || 3001;



io.on('connection', (socket)=>{
	console.log('a user connected');
	socket.on('disconnect', ()=>{
		console.log('user disconnected');
	});
	
});

///////////////poloniex socket connect///////////////////
(function ticker() {

	console.log("\n------------- OPEN CONNECTION ---------------\n");
	const connection = new autobahn.Connection({
		url: 'wss://api.poloniex.com',
		realm: 'realm1',
		max_retries: -1
	});

  	connection.onopen = (session)=>{
	    const marketEvent = (args,kwargs)=>{
	    	console.log(args[0]);
	    	io.emit(args[0], args);
	    	// clientIO.emit('stream', args);
		}
	    session.subscribe('ticker', marketEvent);

	    setTimeout(function () {
	      console.log("\n------------- CLOSING CONNECTION ---------------\n");
	      connection.close();
	    }, 300000);

	};

	connection.open();

	setTimeout(ticker, 300200);
})();

/////////////server////////////
routes(app);

server.listen(port, '0.0.0.0', ()=>{
	console.log('\n' + '**********************************');
	console.log('SCAN Server listening on port ' + port);
	console.log('**********************************' + '\n');
});


